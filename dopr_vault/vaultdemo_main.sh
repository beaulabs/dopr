#!/bin/bash

# This script provides a menu based selection to perform Vault actions during a demo
# run locally from a laptop
#
# NOTE: IF YOU UPLOAD THIS TO VCS ENSURE YOU HAVE SCRUBBED SENSITIVE DATA FROM THE
# SCRIPT YOU MAY HAVE DESIGNATED. EG - OKTATOKEN
#
#
# version: 1.0
# create date: 17 June 2020
# orig create date: 10 June 2019

#######################
#   GLOBAL VARIABLES  #
#######################

# Set the type speed for automating appearance of typing command
export DOCKERENV="$(pwd)"
export TYPE="pv -qL $((40))"
export PROMPT="$ "
export TCOLOR=$(tput setaf 11)

# Uncomment and fill in variables as needed.

# export DEMODOMAIN="<INSERT DEMO DOMAIN FOR PKI>"
# export OKTAORG="<INSERT OKTA ORG>"
# export OKTATOKEN="<INSERT OKTA TOKEN>"
# export DATABASE="<POSTGRES SQL DB TO USE>"
# export DBUSER="<INSERT DB USER>"
# export DBPASS="<INSERT DB PASSWORD>"
# export USER1="<INSERT USER1 NAME>"
# export USER1PASSWD="<INSERT USER1 PASSWORD>"
# export USER2="<INSERT USER2 NAME>"
# export USER2PASSWD="<INSERT USER2 PASSWORD>"

#######################
#      FUNCTIONS      #
#######################

####################
#  Process Checks  #
####################

# This function performs checks to ensure Vault process(es) is/are running before executing demo action.

process_checks() {

    if grep -Fxq "$INPUT" vvolume/data/trackruns.txt; then
        clear
        echo "This action has been already been run..."
        echo "Please select another demo action to perform."
        sleep 2
        menu
    fi

}

###################
# Static  Secrets #
###################

static_secrets() {

    process_checks
    vscripts/vaultdemo_static_secrets.sh
    menu
}

###################
# Dynamic Secrets #
###################

dynamic_secrets() {

    process_checks
    vscripts/vaultdemo_dynamic_secrets.sh
    menu
}

###################
#     Transit     #
###################

transit_secrets() {

    process_checks
    vscripts/vaultdemo_transit_secrets.sh
    menu

}

###################
#     PKI/CA      #
###################

pki_secrets() {

    process_checks
    vscripts/vaultdemo_pki_secrets.sh
    menu

}

###################
#    User Pass    #
###################

userpass_auth() {

    process_checks
    vscripts/vaultdemo_userpass.sh
    menu

}

###################
#    Okta MFA     #
###################

okta_auth() {

    process_checks
    vscripts/vaultdemo_okta_mfa.sh
    menu

}

#######################################
#   END TO END  - Transit / Database  #
#######################################

endtoend_transitdb() {

    process_checks
    vscripts/vaultdemo_endtoend_transitdb.sh
    menu

}

######################################
#   END TO END  - PKI CA / Website  #
#####################################

endtoend_pkiweb() {

    process_checks
    vscripts/vaultdemo_endtoend_pkiweb.sh
    menu

}

###################
#   Namespaces    #
###################

namespaces() {
    process_checks
    vscripts/vaultdemo_namespaces.sh
    menu

}

################
#  Sentinel    #
################

sentinel() {
    process_checks
    vscripts/vaultdemo_sentinel.sh
    menu

}

##########################
#   Disaster Recovery    #
##########################

disaster_recovery() {
    process_checks
    vscripts/vaultdemo_disasterrecovery.sh
    menu

}

##########################
#   Password Rotation    #
##########################

password_rotation() {
    read -rsn1 -p "Press any key to return to menu..."
    menu
}

##############################
#   Performance Replication  #
##############################

performance_replication() {
    read -rsn1 -p "Press any key to return to menu..."
    menu
}

# This function stands up 2 Vault clusters and 2 Consul clusters for backend storage in a containerized
# environment. This is used for a more advanced demo showing performance replication and mount filters.

###################
#   Fast Static   #
###################

# This runs the static secrets commands to set Vault up with initial secrets values
fast_static() {
    process_checks
    vscripts/vaultdemo_faststatic.sh
    menu
}

###################
#   Root Token    #
###################

root_token() {
    clear
    echo "Vault Cluster 1 root token: $(jq -r '.root_token' vvolume/data/vc1.init)"
    echo ""
    echo "Vault Cluster 2 root token: $(jq -r '.root_token' vvolume/data/vc2.init)"

    echo ""
    read -rsn1 -p "Press any key to return to menu..."
}

###################
#  Breakout CLI   #
###################

# This function is a placeholder. Not sure if it will be used.
breakout() {
    clear
    echo "Opening new terminal for manual interaction with Vault."
    #osascript -e "tell application \"Terminal\" to do script \"cd $(pwd); eval $(././vaultsingle/loadenv.sh); vault login $TOKENVC1\""
    osascript -e "tell application \"Terminal\" to activate" -e "tell application \"Terminal\" to do script \"cd $(pwd); source vvolume/data/loadenv.sh; vault login $TOKENVC1\"" >/dev/null 2>&1
    sleep 2
}

##############################
#  Reset Vault Environment   #
##############################

# For testing purposes want an easier way to "turn back time" and return Vault back to
# original state after it was initialized so you don't have to stop and start containers
# and go through the whole process of start/initialize/unseal etc. Yes there are probably
# more efficient ways to do this, but this is a demo environment and it's late, and I'm
# not a professional coder, so there's that.

reset_vault_environment() {

    clear
    echo "RESET VAULT ENVIRONMENT"
    echo "-----------------------"
    echo ""
    echo "This will reset the Vault environment to \"factory settings\" to be able to"
    echo "run through the core demo options again without having to restart the Vault"
    echo "environment from scratch."
    echo ""
    echo "--------------------------------------------------------------"
    echo "-----NOTE: IF YOU HAVE RUN THE DISASTER RECOVERY DEMO YOU-----"
    echo "-----MUST QUIT THE VAULT ENVIRONMENT AND RESTART CLEAN--------"
    echo "--------------------------------------------------------------"
    sleep 2
    source vscripts/exportvc1.sh
    vault login $TOKENVC1 >/dev/null
    echo "Resetting Vault to clean state..."
    vault secrets disable labsecrets/
    vault secrets disable database/
    vault secrets disable transit/
    vault secrets disable pki/
    vault secrets disable pki_int/
    vault namespace delete team1/ &>/dev/null
    vault namespace delete team2/ &>/dev/null
    vault delete sys/policies/egp/time-check
    vault auth disable userpass/
    vault auth disable okta/
    sudo security find-certificate -c $DEMODOMAIN &>/dev/null
    if [ $? == 0 ]; then
        sudo security delete-certificate -t -c $DEMODOMAIN
    fi
    >vvolume/data/trackruns.txt
    echo ""
    echo ""
    echo "Core demo environment options have been reset."
    read -rsn1 -p "Press any key to continue..."
    menu
}

###################
#      Trap       #
###################

# This function is a placeholder. Want to add trapping to break out of demo
# demo elements and return to parent menu.

interrupt() {
    echo "*********** Caught Interrupt ***********"
    kill -TERM "$child" 2>/dev/null
}

###################
#      Quit       #
###################

quit() {
    clear
    # Shutdown containers
    docker-compose -f vdocker/docker-compose.yml down
    # Clean up and remove dynamic data generated during demo
    rm -Rf vvolume/data/*
    # Clean up the database
    psql -U $DBUSER -h localhost -d $DATABASE -c "DROP SCHEMA IF EXISTS thelab CASCADE" >/dev/null 2>&1
    # Reset SQL statement for dynamic database credentials and transit-db end to end demo portions
    sed -i '' "s/$DBUSER/dbuser/g" vdemofiles/revoke.sql
    # Clean up certificate store
    sudo security find-certificate -c $DEMODOMAIN &>/dev/null
    if [ $? == 0 ]; then
        sudo security delete-certificate -t -c $DEMODOMAIN
    fi
    # Return to parent directory
    cd ../
    echo "Your secrets are safe with me..."
    echo "Have a nice day..."
    sleep 1
    exit

}

###################
#      Menu      #
###################

menu() {
    while [[ $INPUT != [Qq] ]]; do
        clear
        echo "# DOPR : VAULT"
        echo "----------------------------------------------"
        echo ""
        echo "Welcome to the Vault demo environment."
        echo "Please select a demo from the menu below:"
        echo ""
        echo "1) Static Secrets"
        echo "2) Dynamic Secrets - Database"
        echo "3) Transit - EaaS"
        echo "4) PKI Secrets - Certificate Authority"
        echo "5) User Based Authoritzation"
        echo "6) Okta Based Authorization with MFA"
        echo "7) End To End Operations - Transit / Database"
        echo "8) End To End Operations - PKI CA / Website"
        echo "9) Namespaces"
        echo "10) Sentinel"
        echo "11) Disaster Recovery"
        echo "----------------------------------------------"
        echo "F) Fast Static Secrets Set Up"
        echo "R) Show root tokens"
        echo "RS) Reset Vault Environment"
        echo "Q) Quit Vault Demo Environment"
        echo ""
        echo "Enter selection and hit return..."
        read INPUT

        case $INPUT in
        1)
            static_secrets
            ;;
        2)
            dynamic_secrets
            ;;
        3)
            transit_secrets
            ;;
        4)
            pki_secrets
            ;;
        5)
            userpass_auth
            ;;
        6)
            okta_auth
            ;;
        7)
            endtoend_transitdb
            ;;
        8)
            endtoend_pkiweb
            ;;
        9)
            namespaces
            ;;
        10)
            sentinel
            ;;
        11)
            disaster_recovery
            ;;
        F | f)
            fast_static
            ;;
        R | r)
            root_token
            ;;
        Reset | reset | RS | rs)
            reset_vault_environment
            ;;
        Q | q)
            quit
            ;;
        *)
            echo "Invalid selection. Please choose from the available options."
            sleep 1
            ;;
        esac

    done

}

#######################
#        MAIN         #
#######################

# Clear the screen to start fresh
clear

# Before Vault demos can be run, must have environment up and running.
# Note this takes about 40 seconds - induced delays are required for clusters
# to start and stablize.
touch vvolume/data/trackruns.txt
echo "Initializing Vault cluster environment for demo. Please standby..."
echo $DOCKERENV
echo "$(pwd)"
docker-compose -f vdocker/docker-compose.yml up -d
./vaultdemo_initialize_clusters.sh

# Run main function
menu
