#!/usr/bin/env bash

# This script is to be used in conjunction with the Dopr Vault demo. It will run post
# "docker-compose up" to initialize and configure the containers with the appropriate
# settings.

# Version: 1.0
# Date: 28 Aug 2019

#######################
#      FUNCTIONS      #
#######################

########################
#     Start Consul     #
########################

# This function starts Consul as a server on the container

servicestart_consul() {

    echo "Starting up Consul server $KEY"
    docker exec -d $KEY opt/shared/config/server/vaultdemo_servicestart_consul.sh
    docker top $KEY | grep consul
    if [[ $? == 0 ]]; then
        echo "Consul on $KEY successfully started..."
    else
        echo "$KEY Error: Consul did not successfully start..."
    fi

}

#######################
#     Start Vault     #
#######################

# This function starts Vault as a server and Consul agent on the container

servicestart_vault() {

    echo "Starting up Vault server $KEY"
    docker exec -d $KEY opt/shared/config/server/vaultdemo_servicestart_vault.sh
    docker top $KEY | grep vault
    if [[ $? == 0 ]]; then
        echo "Vault on $KEY successfully started..."
    else
        echo "$KEY Error: Vault did not successfully start..."
    fi

}

########################
#   Initialize Vault   #
########################

# Initialize one of the Vault servers to gain necessary unseal key and root token. When running in HA you only
# have to do this against one of the Vault servers in the cluster

initialize_vault() {

    echo "Initializing VC1"
    #curl -k -s --request POST --data @vvolume/config/server/vault_init.json https://localhost:10101/v1/sys/init | jq | tee vvolume/data/vc1.init #>/dev/null 2>&1

    curl -s --request POST --data '{"secret_shares": 1, "secret_threshold": 1}' https://localhost:${servers[vc1s1]}/v1/sys/init | jq | tee vvolume/data/vc1.init #>/dev/null 2>&1
    sleep 3

    echo ""
    echo "Initializing VC2"

    curl -s --request POST --data '{"secret_shares": 1, "secret_threshold": 1}' https://localhost:${servers[vc2s1]}/v1/sys/init | jq | tee vvolume/data/vc2.init #>/dev/null 2>&1
    sleep 3

}

####################
#   Unseal Vault   #
####################

# Unseal all Vault servers in clusters' 1 and 2.

unseal_vault() {

    for KEY in "${!servers[@]}"; do

        if [[ $KEY == vc1* ]]; then
            echo "Unsealing Vault Cluster 1 - Server: $KEY"
            curl -s --request PUT --data '{"key": '$(jq '.keys[]' vvolume/data/vc1.init)'}' https://localhost:${servers[$KEY]}/v1/sys/unseal | jq
        elif [[ $KEY == vc2* ]]; then
            echo "Unsealing Vault Cluster 2 - Server: $KEY"
            curl -s --request PUT --data '{"key": '$(jq '.keys[]' vvolume/data/vc2.init)'}' https://localhost:${servers[$KEY]}/v1/sys/unseal | jq
        fi
    done

}

#####################
#   License Vault   #
#####################

# License all Vault instances across both cluster 1 and 2 to enable enterprise features. Remember when using HA, you use the root token
# obtained during initialization access the other Vault members of that cluster. Also you only get 30 mins on
# Vault enterprise binary before it reseals.

license_vault() {

    for KEY in "${!servers[@]}"; do

        if [[ $KEY == vc1* ]]; then
            echo "Licensing Vault Cluster 1 - Server: $KEY"
            curl --header "X-Vault-Token: $(jq -r '.root_token' vvolume/data/vc1.init)" --request PUT --data @vvolume/license/vault_license.json https://localhost:${servers[$KEY]}/v1/sys/license
        elif [[ $KEY == vc2* ]]; then
            echo "Licensing Vault Cluster 2 - Server: $KEY"
            curl --header "X-Vault-Token: $(jq -r '.root_token' vvolume/data/vc2.init)" --request PUT --data @vvolume/license/vault_license.json https://localhost:${servers[$KEY]}/v1/sys/license
        fi
    done

}

#####################
#  License Consul   #
#####################

# Not really needed as Consul enterprisee binary gives you 6 hours of play time before locking out
# unlike Vault's 30 mins, however good practice to go ahead and license in case you want to leave
# environment up and running.

license_consul() {

    for KEY in "${!servers[@]}"; do

        if [[ $KEY == cc1s1 ]]; then
            echo "Licensing Consul Clustere 1 - Server: $KEY"
            curl --request PUT --data @vvolume/license/consul.license http://127.0.0.1:${servers[$KEY]}/v1/operator/license | jq
        elif [[ $KEY == cc2s1 ]]; then
            echo "Licensing Consul Cluster 2 - Server: $KEY"
            curl --request PUT --data @vvolume/license/consul.license http://127.0.0.1:${servers[$KEY]}/v1/operator/license | jq
        fi

    done
}

#####################
#    Webservers     #
#####################

# Valt Demos (as well as Consul Demos) can utilize a webserver(s). This function configures and starts
# and configures the webserver service on webserver containers

webservers() {

    for KEY in "${!servers[@]}"; do

        if [[ $KEY == web* ]]; then
            echo "Starting up web server: $KEY"
            docker exec -d $KEY /opt/shared/config/server/vaultdemo_web_setup.sh
        fi

    done

}

#####################
#   Housekeeping    #
#####################

housekeeping() {
    # Locate the active Vault server node in each Vault cluster
    for KEY in "${!servers[@]}"; do

        if [[ $KEY == vc1* ]] && [[ $(curl -s https://127.0.0.1:${servers[$KEY]}/v1/sys/health | jq '.standby') = "false" ]]; then
            echo "Vault cluster 1 active node found: $KEY"
            echo "$KEY" >vvolume/data/activevc1.node
        elif [[ $KEY == vc2* ]] && [[ $(curl -s https://127.0.0.1:${servers[$KEY]}/v1/sys/health | jq '.standby') = "false" ]]; then
            echo "Vault cluster 2 active node found: $KEY"
            echo "$KEY" >vvolume/data/activevc2.node
        fi

    done

    # Write all server nodes and corresponding localhost port that are part of this Vault demo for reference
    for KEY in "${!servers[@]}"; do
        echo $KEY port: ${servers[$KEY]} >>vvolume/data/allserver.nodes
    done
    sort -o vvolume/data/allserver.nodes vvolume/data/allserver.nodes

    # Create content for exportvc1.sh script for use in setting VAULT_ADDR and VAULT_TOKEN for use in demo
    cat <<EOF >vscripts/exportvc1.sh
    #!/bin/bash
    export VAULT_ADDR=https://localhost:${servers[$(cat vvolume/data/activevc1.node)]}
    export TOKENVC1=$(jq -r '.root_token' vvolume/data/vc1.init)
EOF

    cat <<EOF >vscripts/exportvc2.sh
    #!/bin/bash
    export VAULT_ADDR=https://localhost:${servers[$(cat vvolume/data/activevc2.node)]}
    export TOKENVC2=$(jq -r '.root_token' vvolume/data/vc2.init)
EOF

}

open_vaultui() {

    # Use osascript to open browser and tabs to Vault cluster active node UI and cooresponding Consul cluster
    osascript -e 'tell application "Firefox" to activate' -e 'tell application "Firefox" to open location "https://localhost:'${servers[$(cat vvolume/data/activevc1.node)]}'/ui"' \
        -e 'tell application "Firefox" to open location "http://localhost:10104/ui"'
}

#######################
#        MAIN         #
#######################

# Declare an associative array to read in both Vault and Consul hostnames and cooresponding ports for configuration
declare -A servers=()
while read -r a b; do servers["$a"]="$b"; done < <(docker ps --format="{{.Names}}: {{.Ports}}" | sort | awk -F ":|->" '{print $1, $3}')

clear
echo "STARTING CONSUL SERVERS FOR VAULT STORAGE BACKEND SUPPORT"
echo "--------------------------------------------------------------"

for KEY in "${!servers[@]}"; do
    if [[ $KEY == c* ]]; then
        echo "Processing Consul Server: $KEY"
        servicestart_consul $KEY
    fi
done

# Allow Consul servers to start up, discover and create quorum / leader
echo ""
echo "Standby while Consul clusters complete start up tasks (create quorum, elect leader across both clusters, etc)..."
sleep 10

echo "STARTING VAULT SERVERS"
echo "--------------------------------------------------------------"

for KEY in "${!servers[@]}"; do

    if [[ $KEY == v* ]]; then
        echo "Processing Vault Server: $KEY"
        servicestart_vault $KEY
    fi
done

# Allow Vault servers to fully start up, discover storage, create HA cluster and let consul agent settle
echo ""
echo "Standby while Vault clusters complete start up tasks (start and discover storage across both clusters, etc)..."
sleep 20

echo ""
echo "INITIALIZING VAULT CLUSTERS"
echo "---------------------------------------------------------------"
echo ""
initialize_vault

echo ""
echo "UNSEALING VAULT CLUSTERS"
echo "---------------------------------------------------------------"
echo ""
unseal_vault

echo ""
echo "LICENSING VAULT CLUSTERS"
echo "---------------------------------------------------------------"
echo ""
license_vault

echo ""
echo "LICENSING CONSUL CLUSTERS"
echo "---------------------------------------------------------------"
echo ""
license_consul

echo ""
echo "FINAL HOUSE KEEPING"
echo "---------------------------------------------------------------"
echo ""
echo "Locating Vault active nodes in cluster and creating reference files for use..."
housekeeping
webservers
echo ""
echo "OPENING VAULT DEMO ENVIRONMENT"
echo "---------------------------------------------------------------"
echo ""
echo "Opening Vault UI..."

source vscripts/exportvc1.sh
vault login $TOKENVC1
sleep 2
open_vaultui

echo "Completed Vault and Consul start-up, initialization and configuration."
echo "Your demo environmeent is ready to be used. Returning you to Vault menu."
