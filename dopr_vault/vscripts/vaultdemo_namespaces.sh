#!/bin/bash

# This script runs the Namespaces demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 10 Mar 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "9" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

# Enable the Okta authentication method
echo "ENABLE NAMESPACES IN VAULT"
echo "--------------------------------------------------------------"
echo ""
echo "You may have operational needs to control access to secrets at a more"
echo "granular level than just via policies. You may require tenant"
echo "isolation, but still give operational control to teams to allow"
echo "to be self-sufficient and consume their own secrets."
echo ""
echo "Namespaces allow you to partition and offer isolated environments."
echo "You can create and have a \"vault within a vault\" functionality."
echo ""
echo "COMMAND: vault namespace create team1"
echo ""
echo ""
echo "$TCOLOR vault namespace create team1" | $TYPE
echo ""
tput sgr0

vault namespace create team1

echo ""
echo ""
echo "As always you can use the API to create a namespace as well."
echo "We'll create a second namespace via the API:"
echo ""
echo "COMMAND: curl --header \"X-Vault-Token: \$TOKENVC1\" --request POST $VAULT_ADDR/v1/sys/namespaces/team2"
echo ""
echo "$TCOLOR curl --header "X-Vault-Token: $TOKENVC1" --request POST $VAULT_ADDR/v1/sys/namespaces/team2" | $TYPE
echo ""

curl --header "X-Vault-Token: $TOKENVC1" --request POST $VAULT_ADDR/v1/sys/namespaces/team2 | jq

echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo "Once you have your namespaces created you can easily view them via CLI or API:"
echo ""
echo "COMMAND: vault namespace list"
echo ""
echo ""
echo "$TCOLOR vault namespace list" | $TYPE
echo ""

vault namespace list

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

# Configure Vault to communicate to Okta
echo "CONFIGURE POLICIES ON THE NAMESPACES"
echo "--------------------------------------------------------------"
echo ""
echo "Just as when you first configured Vault, you set policies on the namespace(s)"
echo "that are used to grant a role and permissions. The difference is you explicity"
echo "tell Vault which namespace the policy will apply to."
echo ""
echo "COMMAND: vault policy write -namespace=<namespace> <policy name> vdemofiles/<policy file name>.hcl"
echo ""
echo "$TCOLOR vault policy write -namespace=team1 namespace_team1 vdemofiles/namespace_team1.hcl" | $TYPE
echo "$TCOLOR vault policy write -namespace=team2 namespace_team2 vdemofiles/namespace_team2.hcl" | $TYPE
echo ""
tput sgr0

vault policy write -namespace=team1 namespace_team1 vdemofiles/namespace_team1.hcl
vault policy write -namespace=team2 namespace_team2 vdemofiles/namespace_team2.hcl

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

# Configure the appropriate policy to be attached based on group membership
echo "ENABLE AN AUTHENTICATION METHOD"
echo "--------------------------------------------------------------"
echo ""
echo "Similarly as above, one must authenticate to Vault before authorization occurs"
echo "and Vault assigns a token with policies attached. Once again you enable the"
echo "authentication entry point against the namespace."
echo ""
echo "In this example, we will enable a local username/password method on our two"
echo "created team namespaces."
echo ""
echo "COMMAND: vault auth enable -namespace=<namespace> userpass"
echo ""
echo ""
echo "$TCOLOR vault auth enable -namespace=team1 userpass" | $TYPE
echo "$TCOLOR vault auth enable -namespace=team2 userpass" | $TYPE
echo ""
tput sgr0

vault auth enable -namespace=team1 userpass
vault auth enable -namespace=team2 userpass

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "CREATE THE USERS"
echo "--------------------------------------------------------------"
echo ""
echo "Once the namespace authentication method has been created we can create"
echo "users."
echo ""
echo "In this example we'll create logins for our two favorite lab characters."
echo ""
echo ""
echo "COMMAND: vault write -namespace=<namespace> auth/userpass/users/<name of user> password=<password> policies=<policy name>"
echo ""
echo ""
echo "$TCOLOR vault write -namespace=team1 auth/userpass/users/$USER1 password=\"honeydew\" policies=\"namespace_team1\"" | $TYPE
echo "$TCOLOR vault write -namespace=team2 auth/userpass/users/$USER2 password=$USER2PASSWD policies=\"namespace_team2\"" | $TYPE
echo ""
tput sgr0

vault write -namespace=team1 auth/userpass/users/$USER1 password=$USER1PASSWD policies="namespace_team1"
vault write -namespace=team2 auth/userpass/users/$USER2 password="meep" policies="namespace_team2"

echo ""
echo "Finally we'll enable and post a few secrets into the namespaces."
echo ""
echo ""
echo "$TCOLOR vault secrets enable -namespace=team1 -path=\"team1apps\" kv" | $TYPE
echo "$TCOLOR vault secrets enable -namespace=team2 -path=\"team2apps\" kv" | $TYPE
echo "$TCOLOR vault secrets enable -namespace=team2 -path=\"team2salary\" kv" | $TYPE
echo "$TCOLOR vault kv put -namespace=team1 team1apps/app1 password=\"supersecret\"" | $TYPE
echo "$TCOLOR vault kv put -namespace=team2 team2apps/app2 password=\"soverysupersecret\"" | $TYPE
echo "$TCOLOR vault kv put -namespace=team2 team2salary/kermit-ceo salary=\"1,000,000\"" | $TYPE
tput sgr0

vault secrets enable -namespace=team1 -path="team1apps" kv
vault secrets enable -namespace=team2 -path="team2apps" kv
vault secrets enable -namespace=team2 -path="team2salary" kv
vault kv put -namespace=team1 team1apps/app1 password="supersecret"
vault kv put -namespace=team2 team2apps/app2 password="soverysupersecret"
vault kv put -namespace=team2 team2salary/kermit-ceo salary="1,000,000"

echo ""
echo ""
echo "Log in as the users above to note the tenant isolation."
echo "This concludes the namespaces portion of the demo."

# Have osascript open Vault UI to page with namespace and authentication method set to userpass
osascript -e 'tell application "Firefox" to activate' -e 'tell application "Firefox" to open location "https://localhost:10102/ui/vault/auth?namespace=team1&with=userpass"' \
    -e 'tell application "Firefox" to open location "https://localhost:10103/ui/vault/auth?namespace=team2&with=userpass"'

read -rsn1 -p "Press any key to return to menu..."
