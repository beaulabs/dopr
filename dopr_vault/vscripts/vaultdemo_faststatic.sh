#!/bin/bash

# This script runs the Vault static secrets demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "f" >>vvolume/data/trackruns.txt
echo "1" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null
vault secrets enable -path="labsecrets" kv >/dev/null
vault kv put labsecrets/apikeys/googlemain apikey="master-api-key-111111" >/dev/null
curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"gvoiceapikey": "walkie-talkie-222222"}' $VAULT_ADDR/v1/labsecrets/apikeys/googlevoice >/dev/null
vault kv put labsecrets/webapp username="$USER2" password=$USER2PASSWD >/dev/null
vault kv put labsecrets/labinfo @vdemofiles/data.json >/dev/null
vault kv put labsecrets/lab_keypad code="12345" >/dev/null
vault kv put labsecrets/lab_room room="A113" >/dev/null
curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"gmapapikey": "where-am-i-??????"}' $VAULT_ADDR/v1/labsecrets/apikeys/googlemaps >/dev/null
