#!/bin/bash

# This script runs the Vault PKI secrets demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "4" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

# Create variable to set stripped domain name for use in setting up role for subdomains
SUBDOMAIN=$(echo $DEMODOMAIN | sed 's/[.].*//')

echo "PKI - USING VAULT AS A CERTIFICATE AUTHORITY"
echo "--------------------------------------------------------------"
echo ""
echo "Vault's power comes from dynamic secrets servicing. We manage intentions"
echo "(e.g. web server needs database access (shown earlier) or needs a new TLS certificate) instead of"
echo "managing credentials (e.g. authenticating data provided to web server requiring database access)."
echo ""
echo "Dynamic secrets can be applied to public key certificats as well to generate short lived certificates"
echo "which allows for generating on-demand, rotating and revoking automatically."
echo ""
echo "This allows services to acquire certificates without using a manual process of generating a private key"
echo "and Certificate Signing Request, submitting to a Certificate Authority and then waiting for verification"
echo "and signing process to complete."
echo ""
echo "To illustrate this the following workflow will be followed:"
echo ""
echo "1) Enable the PKI engine."
echo "2) Create both a Root CA and an Intermediate CA."
echo "3) Create a role for issuing a certificate."
echo "4) Request a certificate for use."
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear
# Enable vault pki engine

echo "ENABLE THE PKI SECRETS ENGINE"
echo "--------------------------------------------------------------"
echo ""
echo "Enabling the PKI secrets engine is the same as shown during the static and dynamic secrets demos."
echo ""
echo "$TCOLOR vault secrets enable pki" | $TYPE
tput sgr0
echo ""
echo ""

vault secrets enable pki

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
echo "Once the PKI engine has been enabled we'll set a default max-lease-ttl value..."

echo ""
echo "$TCOLOR vault secrets tune -max-lease-ttl=87600 pki" | $TYPE
tput sgr0
echo ""
echo ""

# Tune pki engine for max ttl of 87600
vault secrets tune -max-lease-ttl=87600 pki

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

clear

echo "GENERATE THE ROOT CA"
echo "--------------------------------------------------------------"
echo ""
echo "We can now go about setting Vault as a Root CA by generating a root certificate and"
echo "saving it off as CA_cert.crt."
echo ""
echo "This is a self-signed CA certificate and private key."
echo ""
echo ""
echo "$TCOLOR vault write -field=certificate pki/root/generate/internal common_name=\"$DEMODOMAIN\" ttl=87600h >vvolume/data/CA_cert.crt" | $TYPE
tput sgr0
echo ""
echo ""

# Generate root cert and save the cert in CA_cert.crt
vault write -field=certificate pki/root/generate/internal common_name="$DEMODOMAIN" ttl=87600h >vvolume/data/CA_cert.crt

echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

echo ""
echo ""
echo "Once the root certifcate has been successfully generated we can have Vault configure the"
echo "appropriate CA and CRL URLs to be used."
echo ""
echo ""

echo "$TCOLOR vault write pki/config/urls issuing_certificates=\"$VAULT_ADDR/v1/pki/ca\" crl_distribution_points=\"$VAULT_ADDR/v1/pki/crl\"" | $TYPE
tput sgr0
echo ""
echo ""

# configure the CA and CRL URLs
vault write pki/config/urls issuing_certificates="$VAULT_ADDR/v1/pki/ca" crl_distribution_points="$VAULT_ADDR/v1/pki/crl"

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
# ROOT CA HAS BEEN SET UP. NOW ONTO THE INTERMEDIATE CA

clear

echo "GENERATE THE INTERMEDIATE CA"
echo "--------------------------------------------------------------"
echo ""
echo "After the Root CA has been created we can create the Intermediate CA using the new Root CA."
echo "Similar to the Root CA we must enable a PKI secrets engine, although on a new path."
echo "As well as set a default maximum time-to-live."
echo ""
echo ""
echo "$TCOLOR vault secrets enable -path=pki_int pki" | $TYPE
tput sgr0
echo ""
echo ""

# Generate root cert and save the cert in CA_cert.crt
vault secrets enable -path=pki_int pki

echo ""
echo ""
echo "$TCOLOR vault secrets tune -max-lease-ttl=43800h pki_int" | $TYPE
tput sgr0
echo ""
echo ""

# Tune intermediate CA to max ttl of 43800
vault secrets tune -max-lease-ttl=43800h pki_int
echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
clear
echo "Next up is to generate the Intermediate CA and save the CSR off for signing by the root certificate."
echo ""
echo ""
echo "$TCOLOR vault write -format=json pki_int/intermediate/generate/internal common_name=\"$SUBDOMAIN Intermediate Authority\" | jq -r '.data.csr' >vvolume/data/pki_intermediate.csr" | $TYPE
tput sgr0
echo ""
echo ""

# Generate an intermediate and save the CSR as pki_intermediate.csr
vault write -format=json pki_int/intermediate/generate/internal common_name="$SUBDOMAIN Intermediate Authority" | jq -r '.data.csr' >vvolume/data/pki_intermediate.csr

echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
echo "With the certificate signing request now ready we'll sign the intermediate certificate with the root certificate"
echo "and save the generated certificate as intermediate.cert.pem."
echo ""
echo ""
echo "$TCOLOR vault write -format=json pki/root/sign-intermediate csr=@vvolume/data/pki_intermediate.csr format=pem_bundle ttl="43800h" | jq -r '.data.certificate' >vvolume/data/intermediate.cert.pem" | $TYPE
tput sgr0
echo ""
echo ""

# Sign the intermediate certificate with the root certificate and save the generated certificate as intermediate.cert.pem
vault write -format=json pki/root/sign-intermediate csr=@vvolume/data/pki_intermediate.csr format=pem_bundle ttl="43800h" | jq -r '.data.certificate' >vvolume/data/intermediate.cert.pem

echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
echo "Finally we'll import the the intermediate certificate back into Vault."
echo ""
echo ""
echo "$TCOLOR vault write pki_int/intermediate/set-signed certificate=@vvolume/data/intermediate.cert.pem" | $TYPE
tput sgr0
echo ""
echo ""

# Once CSR has been signed and the root CA returns a certificate, import back into Vault
vault write pki_int/intermediate/set-signed certificate=@vvolume/data/intermediate.cert.pem

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
# INTERMEDIATE CA HAS BEEN SETUP AND CONFIGURED NOW TO CREATE ROLE

clear

echo "CREATE ROLE"
echo "--------------------------------------------------------------"
echo ""
echo "Certificates require roles. The role is a definition that sets the conditions"
echo "under which a certificate can be generated."
echo ""
echo "Configuration parameters are used to control these conditions. e.g. allowed-domains"
echo ""
echo ""
echo "$TCOLOR vault write pki_int/roles/$SUBDOMAIN-dot-test allowed_domains=\"$DEMODOMAIN\" allow_subdomains=\"true\" max_ttl=\"720h\"" | $TYPE
tput sgr0
echo ""
echo ""

# Create a role that allows for subdomains of the primary
vault write pki_int/roles/$SUBDOMAIN-dot-test allowed_domains="$DEMODOMAIN" allow_subdomains="true" max_ttl="720h"

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

clear
# REQUEST CERTIFICATES
echo "REQUEST CERTIFICATES"
echo "--------------------------------------------------------------"
echo ""
echo "Now that the certificate authorities have been created and roles configured,"
echo "to request a certificate is as simple as hitting the roles endpoint via an API call."
echo ""
echo ""
echo "$TCOLOR curl --header \"X-Vault-Token: $TOKENVC1\" --request POST --data '{\"common_name\": \"cat.$DEMODOMAIN\", \"ttl\": \"10m\"}' $VAULT_ADDR/v1/pki_int/issue/$SUBDOMAIN-dot-test | jq" | $TYPE
tput sgr0
echo ""
echo ""

# Request a new certificate for the subdomain based on the domain role
#vault write pki_int/issue/$SUBDOMAIN-dot-test common_name="cat.$DEMODOMAIN" ttl="24h"
curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"common_name": "cat.'"$DEMODOMAIN"'", "ttl": "10m"}' $VAULT_ADDR/v1/pki_int/issue/$SUBDOMAIN-dot-test | jq

# Generate a root certificate to install on local machine so it doesn't complain about untrusted ca
curl --header "X-Vault-Token: $TOKENVC1" --request GET $VAULT_ADDR/v1/pki/ca/pem >vvolume/data/$SUBDOMAIN.pem

# Convert certificate file to .pem format for OSX keychain trust
# openssl x509 -in vvolume/data/root.crt -out vvolume/data/$SUBDOMAIN.pem -outform PEM
# Inject newly domain .pem file into OS keychain so system and browser will trust the certificate
# This avoids the NET::ERR_CERT_AUTHORITY_INVALID warning in browser
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain vvolume/data/$SUBDOMAIN.pem

echo "This concludes the PKI secrets engine component of the demo."
read -rsn1 -p "Press any key to return to menu..."
# REVOKE CERTIFICATES

# If you need to revoke a certificate you can simply do so with via the serial number

# vault write pki_int/revoke serial_number=24:b1:46:44:d7:fc:63:93:11:3e:cf:a1:91:c2:46:fa:d2:6d:a0:3f

# # To clean up the CRL and empty our revoked certs

# vault write pki_int/tidy tidy_cert_store=true tidy_revoked_certs=true
