#!/bin/bash

# This script runs the DR demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 10 Mar 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "11" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

# Enable the Okta authentication method
echo "ENABLE DISASTER RECOVERY CLUSTER IN VAULT"
echo "--------------------------------------------------------------"
echo ""
echo "Mission critical systems should have a disastery recovery strategy."
echo "Vault Enterprise gives you the capability to protect your primary"
echo "cluster from a catastrophic event and recover quickly and smoothly."
echo ""
echo "The architecture for DR has a leader-follower model. You create an"
echo "alternate Vault cluster and enable it as a DR cluster. This demo"
echo "walks through creating a DR cluster, invoking a failover and return."
echo ""
echo "The first step is to enable DR replication flow on the PRIMARY cluster."
echo ""
echo "COMMAND: vault write -f sys/replication/dr/primary/enable"
echo ""
echo ""
echo "$TCOLOR vault write -f sys/replication/dr/primary/enable" | $TYPE
echo ""
tput sgr0

vault write -f sys/replication/dr/primary/enable

echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
echo "Once the primary cluster has been been enabled for DR replication"
echo "you create a secondary token for use on the designated DR cluster."
echo "This is a wrapping token that is used to enable DR SECONDARY cluster."
echo ""
echo "COMMAND: vault write sys/replication/dr/primary/secondary-token id=\"secondary\""
echo ""
echo "$TCOLOR vault write sys/replication/dr/primary/secondary-token id=\"secondary\"" | $TYPE
echo ""
tput sgr0

vault write sys/replication/dr/primary/secondary-token id="secondary" | tee vvolume/data/secondarytokendr.txt

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear
osascript -e 'tell application "Firefox" to activate' -e 'tell application "Firefox" to open location "https://localhost:10203/ui"'

# Need to source secondary Vault environment and login to complete actions on secondary cluster
source vscripts/exportvc2.sh
vault login $TOKENVC2 >/dev/null

# Configure Vault to communicate to Okta
echo "ENABLE VAULT DR SECONDARY CLUSTER"
echo "--------------------------------------------------------------"
echo ""
echo "To enable the secondary cluster to become a DR secondary cluster"
echo "is as simple as calling the DR replication path and passing it the"
echo "secondary token that was created by the PRIMARY cluster."
echo ""
echo "COMMAND: vault write sys/replication/dr/secondary/enable token=<token value>"
echo ""
echo "$TCOLOR vault write sys/replication/dr/secondary/enable token="$(cat vvolume/data/secondarytokendr.txt | awk 'NR==3{print $2}')"" | $TYPE
echo ""
echo ""
tput sgr0

vault write sys/replication/dr/secondary/enable token="$(cat vvolume/data/secondarytokendr.txt | awk 'NR==3{print $2}')"

echo ""
echo ""
echo "You can now see the secondary cluster has been enabled to be a DR SECONDARY cluster."
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

# Configure the appropriate policy to be attached based on group membership
echo "DECLARE A DISASTER AND PROMOTE THE SECONDARY VAULT CLUSTER"
echo "--------------------------------------------------------------"
echo ""
echo "In this example we will \"declare\" a disaster and promote the secondary"
echo "cluster to become the primary Vault cluster. Note - we do not have an"
echo "automatic failover."
echo ""
echo "To initiate a recovery on the DR secondary (your actual DR cluster) initiate"
echo "a dr-token. This is similar to when you initiate a root token. It will"
echo "generate a nonce that will be combined with your initialization key (from"
echo "when you created your first cluster) as well as a one-time password."
echo ""
echo "COMMAND: vault operator generate-root -dr-token -init"
echo ""
echo ""
echo "$TCOLOR vault operator generate-root -dr-token -init" | $TYPE
echo ""
echo ""
tput sgr0

vault operator generate-root -dr-token -init | tee vvolume/data/secondarynonce.txt

echo ""
echo ""
echo "Depending on how many Shamir keys you have, you give the nonce to each"
echo "unseal key holder."
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
echo ""
echo ""
echo "Combine the nonce with each unseal key to generate the encoded token..."
echo ""
echo "COMMAND: vault operator generate-root -dr-token -nonce=<nonce value> <unseal key>"
echo ""
echo ""
echo "$TCOLOR vault operator generate-root -dr-token -nonce=$(cat vvolume/data/secondarynonce.txt | awk 'NR==1{print $2}') $(jq -r '.keys[]' vvolume/data/vc1.init)" | $TYPE
echo ""
tput sgr0

vault operator generate-root -dr-token -nonce=$(cat vvolume/data/secondarynonce.txt | awk 'NR==1{print $2}') $(jq -r '.keys[]' vvolume/data/vc1.init) | tee vvolume/data/secondaryencodedtoken.txt

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

echo ""
echo ""
echo "The next step is to decode the token that was just received."
echo ""
echo "COMMAND: vault operator generate-root -dr-token -decode=<encoded token received> -otp=<one time password>"
echo ""
echo ""
echo "$TCOLOR vault operator generate-root -dr-token -decode=$(cat vvolume/data/secondaryencodedtoken.txt | awk 'NR==5{print $3}') -otp=$(cat vvolume/data/secondarynonce.txt | awk 'NR==5{print $2}')" | $TYPE
echo ""
tput sgr0

vault operator generate-root -dr-token -decode=$(cat vvolume/data/secondaryencodedtoken.txt | awk 'NR==5{print $3}') -otp=$(cat vvolume/data/secondarynonce.txt | awk 'NR==5{print $2}') | tee vvolume/data/secondarydropstoken.txt

echo ""
echo ""
echo "This will reveal the dr-token."
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

echo ""
echo ""
echo "Lastly, use the dr-token to promote the DR cluster to become the primary."
echo ""
echo "COMMAND: vault write /sys/replication/dr/secondary/promote dr_operation_token=<dr-token-received>"
echo ""
echo ""
echo "$TCOLOR vault write /sys/replication/dr/secondary/promote dr_operation_token=$(cat vvolume/data/secondarydropstoken.txt)" | $TYPE
echo ""
tput sgr0

vault write /sys/replication/dr/secondary/promote dr_operation_token=$(cat vvolume/data/secondarydropstoken.txt)

echo "This concludes the DR portion of the demo."
read -rsn1 -p "Press any key to return to menu..."
