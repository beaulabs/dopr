#!/bin/bash

# This script runs the Sentinel demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 10 Mar 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "10" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

if ! grep -Fxq "5" vvolume/data/trackruns.txt; then
    echo "Key demo action userpass method not enabled. Missing users. Enabling authentication..."
    vault policy write base vdemofiles/base.hcl
    vault policy write base2 vdemofiles/base2.hcl
    vault auth enable userpass
    vault write auth/userpass/users/$USER2 password="meep" policies="base2"
    vault write auth/userpass/users/$USER1 password=$USER1PASSWD policies="base"
fi

if ! grep -Fxq "1" vvolume/data/trackruns.txt; then
    echo "Key demo action kv engine not enabled. Missing kv data. Enabling kv secrets engine..."
    vault login $TOKENVC1 >/dev/null
    vault secrets enable -path="labsecrets" kv >/dev/null
    vault kv put labsecrets/apikeys/googlemain apikey="master-api-key-111111" >/dev/null
    curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"gvoiceapikey": "walkie-talkie-222222"}' $VAULT_ADDR/v1/labsecrets/apikeys/googlevoice >/dev/null
    vault kv put labsecrets/webapp username="$USER2" password=$USER2PASSWD >/dev/null
    vault kv put labsecrets/labinfo @vdemofiles/data.json >/dev/null
    vault kv put labsecrets/lab_keypad code="12345" >/dev/null
    vault kv put labsecrets/lab_room room="A113" >/dev/null
    curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"gmapapikey": "where-am-i-??????"}' $VAULT_ADDR/v1/labsecrets/apikeys/googlemaps >/dev/null
fi

clear
# Enable the Okta authentication method
echo "ENABLE SENTINEL IN VAULT"
echo "--------------------------------------------------------------"
echo ""
echo "Although Vault provides robust and secure controls via policies,"
echo "there are times that organizations may find they need additional"
echo "capabilities to enforce fine-grained, logic based policies that"
echo "traditional policies will not be able to fully handle."
echo ""
echo "Sentinel is a policy as code framework embedded in Vault enterprise"
echo "that gives you the fine grained policy control. It has two parts."
echo ""
echo "--------------------------------------------------------------"
echo "Role Governing Policies: tied to tokens, identities or groups"

echo "Endpoint Governing Policies: tie to particular paths in Vault"
echo "--------------------------------------------------------------"
echo ""
echo "There are two actions to a Sentinel policy in Vault:"
echo ""
echo "* Write the policy"
echo "* Upload to the policies endpoint with enforcement type"
echo ""
echo "If a Sentinel policy exists, it is checked prior to action"
echo "undertaken by normal ACL policies."
echo ""
echo "In this example we will create a time based policy that will"
echo "restrict access to secrets based on days of the week and hours"
echo "in the day."
echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

# Configure Vault to communicate to Okta
echo "CONFIGURE SENTINEL POLICY"
echo "--------------------------------------------------------------"
echo ""
echo "The example policy we will deploy restricts access to secrets under"
echo "the key value store of labscrets/apikeys."
echo ""

echo "$TCOLOR cat vdemofiles/time-check.sentinel" | $TYPE
echo ""
tput sgr0

cat vdemofiles/time-check.sentinel

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

# Configure the appropriate policy to be attached based on group membership
echo "DEPLOY THE TIME-CHECK SENTINEL POLICY"
echo "--------------------------------------------------------------"
echo ""
echo "Deploying a sentinel policy is similar to deploying ACL policies."
echo "The only difference is Sentinel policies are base64 encoded."
echo ""
echo "COMMAND: vault write sys/policies/egp/time-check policy=\$(base64 policy.sentinel) paths=<path to secrets> enforcement_level=<enforce type>"
echo ""
echo ""
echo "$TCOLOR vault write sys/policies/egp/time-check policy=$(base64 vdemofiles/time-check.sentinel) paths=labsecrets/apikeys enforcement_level=hard-mandatory" | $TYPE
echo ""
tput sgr0

vault write sys/policies/egp/time-check policy=$(base64 vdemofiles/time-check.sentinel) paths=labsecrets/api* enforcement_level=hard-mandatory

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "TEST THE POLICY"
echo "--------------------------------------------------------------"
echo ""
echo "Once the policy is uploaded Vault will begin enforcing automatically."
echo ""
echo "We'll attempt to access the labsecrets/apikeys by logging in as user Bunsen."
echo "NOTE: Root or users with root policies are not affected by Sentinel."
echo "This is by design in case you Vault enters an upset state."
echo ""
echo "COMMAND: vault login -method=userpass username=<username> password=<password>"
echo "COMMAND: vault kv get <path/to/secrets"
echo ""
echo ""
echo "$TCOLOR vault login -method=userpass username=$USER1 password=honeydew" | $TYPE
echo "$TCOLOR vault kv get labsecrets/apikeys/googlemain" | $TYPE
echo ""
tput sgr0

vault login -method=userpass username=$USER1 password=honeydew
echo ""
echo ""
vault kv get labsecrets/apikeys/googlemain

echo ""
echo ""
echo "We can make adjustments real time in the GUI for further testing."
read -rsn1 -p "Demo paused - Correct to allow Sentinel to pass..."
echo ""
echo ""
echo "$TCOLOR vault kv get labsecrets/apikeys/googlemain" | $TYPE
echo ""
tput sgr0

vault kv get labsecrets/apikeys/googlemain

echo ""
echo ""
echo "This concludes the Sentinel portion of the demo."
read -rsn1 -p "Press any key to return to menu..."
