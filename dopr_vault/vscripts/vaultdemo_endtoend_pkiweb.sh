#!/bin/bash

# This script runs the Operational End to End PKI certificate authority with webserver demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "8" >>vvolume/data/trackruns.txt
clear
# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

# Check if the PKI secrets demo has been run prior to this demo. If it hasn't set up the PKI engine
if ! grep -Fxq "4" vvolume/data/trackruns.txt; then
    echo "Key demo action PKI engine not enabled. Prepping certificate authority..."
    SUBDOMAIN=$(echo $DEMODOMAIN | sed 's/[.].*//')
    vault secrets enable pki
    vault secrets tune -max-lease-ttl=87600 pki >/dev/null
    vault write -field=certificate pki/root/generate/internal common_name="$DEMODOMAIN" ttl=87600h >vvolume/data/CA_cert.crt >/dev/null
    vault write pki/config/urls issuing_certificates="$VAULT_ADDR/v1/pki/ca" crl_distribution_points="$VAULT_ADDR/v1/pki/crl" >/dev/null
    vault secrets enable -path=pki_int pki
    vault secrets tune -max-lease-ttl=43800h pki_int >/dev/null
    vault write -format=json pki_int/intermediate/generate/internal common_name="$SUBDOMAIN Intermediate Authority" | jq -r '.data.csr' >vvolume/data/pki_intermediate.csr
    vault write -format=json pki/root/sign-intermediate csr=@vvolume/data/pki_intermediate.csr format=pem_bundle ttl="43800h" | jq -r '.data.certificate' >vvolume/data/intermediate.cert.pem
    vault write pki_int/intermediate/set-signed certificate=@vvolume/data/intermediate.cert.pem >/dev/null
    vault write pki_int/roles/$SUBDOMAIN-dot-test allowed_domains="$DEMODOMAIN" allow_subdomains="true" max_ttl="720h" >/dev/null
    curl -s --header "X-Vault-Token: $TOKENVC1" --request GET $VAULT_ADDR/v1/pki/ca/pem >vvolume/data/$SUBDOMAIN.pem

    # Inject newly domain .pem file into OS keychain so system and browser will trust the certificate
    # This avoids the NET::ERR_CERT_AUTHORITY_INVALID warning in browser
    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain vvolume/data/$SUBDOMAIN.pem
fi
clear

# Showcase end to end use of Vault
echo "END TO END OPERATIONS DEMO - PKI CERT AUTHORITY WITH WEBSERVER"
echo "--------------------------------------------------------------"
echo ""
echo "Across this demo we've shown static, dynamic secrets, encryption as a service and methods of authentication."
echo "This sections walks you through a scenario use case of using Vault in an operational aspect."
echo ""
echo "In this scenario our lab manager Bunsen Honeydew needs to create secure website to showcase his awesome cats."
echo "However, he doesn't want the cost and time of performing a CSR with VeriSign and wishes to use his own"
echo "in-house trusted certificate authority provided by Vault."
echo ""
echo "The following workflow will be followed:"
echo ""
echo "1) Bunsen deployes his website called \"cat.$DEMODOMAIN\" using Nginx initially on unsecured port 80"
echo "2) The Nginx website will then be configured to run securely on port 443"
echo "3) Bunsen will request a dynamic certificate from Vault for the \"cat.$DEMODOMAIN\" domain"
echo "4) The certs will be parsed and injected into Nginx for use, securing his lab's awesome website"
echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."

clear

echo "BUNSEN WANTS TO SEE HIS COOL CATS"
echo "--------------------------------------------------------------"
echo ""
echo "The following index.html will be deployed to Bunsen's webserver..."
echo "/var/www/catapp/index.html"
echo ""
echo "<html>"
echo "<head><title>Meow!</title></head>"
echo "<body>"
echo "<div style=\"width:800px;margin: 0 auto\">"
echo ""
echo "<!-- BEGIN -->"
echo "<center><img src=\"https://placekitten.com/450/450\"></img></center>"
echo "<center><h2>Meow World!</h2></center>"
echo "Welcome to Beau's cat app. Meow meow do what I want because I'm a cat..."
echo "<!-- END -->"
echo ""
echo "</div>"
echo "</body>"
echo "</html>"
echo ""
echo ""
echo "$TCOLOR cp index.html /var/www/catapp/index.html" | $TYPE
echo "$TCOLOR service nginx restart" | $TYPE
tput sgr0
echo ""
echo ""

# Command below is legacy. This is now handled at start up of Vault demo
#docker exec web1 /bin/bash -c '/opt/shared/config/server/vaultdemo_web_setup.sh'
sleep 2
osascript -e 'tell application "Firefox" to activate' -e 'tell application "Firefox" to open location "http://cat.'$DEMODOMAIN'/"'

read -rsn1 -p "Demo paused - Press any key to continue..."

clear

echo "BUNSEN REALIZES HE NEEDS TO KEEP HIS CATS SECURE"
echo "--------------------------------------------------------------"
echo ""
echo "Once the cats are up and looking around, Bunsen wants to serve the catapp securely."
echo "He'll call Vault to obtain a dynamic cert..."
echo ""
echo "$TCOLOR curl --header \"X-Vault-Token: $TOKENVC1\" --request POST --data '{\"common_name\": \"cat.$DEMODOMAIN\", \"ttl\": \"1h\"}' $VAULT_ADDR/v1/pki_int/issue/beaulabs-dot-test | jq . >vvolume/data/nginxcerts.json" | $TYPE
tput sgr0

curl --header "X-Vault-Token: $TOKENVC1" --request POST --data '{"common_name": "cat.'"$DEMODOMAIN"'", "ttl": "1h"}' $VAULT_ADDR/v1/pki_int/issue/beaulabs-dot-test | jq . >vvolume/data/nginxcerts.json

echo ""
echo ""
# Extract certificate information from file to create private key and public ssl cert
echo "Once the certificate information has been received it is parsed for the private key..."
echo ""
echo "$TCOLOR jq -r '.data.private_key' vvolume/data/nginxcerts.json >vvolume/data/key" | $TYPE
tput sgr0

jq -r '.data.private_key' vvolume/data/nginxcerts.json >vvolume/data/key

echo ""
echo ""
echo "Next it is parsed for the public cert..."
echo ""
echo "$TCOLOR jq -r '.data.certificate' vvolume/data/nginxcerts.json >>vvolume/data/ssl_cert" | $TYPE
tput sgr0

jq -r '.data.certificate' vvolume/data/nginxcerts.json >>vvolume/data/ssl_cert

echo ""
echo ""
echo "Lastly it is parsed for the cert chain..."
echo ""
echo "$TCOLOR jq -r '.data.issuing_ca' vvolume/data/nginxcerts.json >>vvolume/data/ssl_cert" | $TYPE
tput sgr0

jq -r '.data.issuing_ca' vvolume/data/nginxcerts.json >>vvolume/data/ssl_cert

# Stage certificates into location for nginx to pick up upon restart
echo ""
echo ""
echo "Bunsen then injects the certs into his webserver and reloads the website..."
echo ""
echo "$TCOLOR mv ssl_cert key /etc/nginx/cert" | $TYPE
echo "$TCOLOR service nginx restart" | $TYPE
tput sgr0

docker exec web1 /bin/bash -c '/opt/shared/config/server/vaultdemo_web_setup443.sh'

echo ""
echo ""
echo "NOTE: YOUR MACHINE IS NOT GOING TO TRUST THE CERTIFICATE UNLESS IT IS PART OF ITS KEYCHAIN"
echo "Install vvolume/data/root.crt into your Keychain Access and set the cert to be trusted."
echo ""
echo "Finally, open your browser to https://cat.$DEMODOMAIN"
echo ""
echo ""
read -rsn1 -p "This concludes the end-to-end operational use demo. Press any key to return to menu..."
