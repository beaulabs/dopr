#!/bin/bash

# This script runs the Operational End to End Transit with Database demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

source vscripts/exportvc1.sh
echo "7" >>vvolume/data/trackruns.txt
clear

# If this is run after the vaultdemo_userpass demo Vault thinks it is logged in as $USER1 in this terminal session.
# Need to reauthenticate as root to continue the demo
vault login $TOKENVC1 >/dev/null

# Quick check to see if Dynamic Secrets has been executed. If not, prep database for end to end actions
if ! grep -Fxq "2" vvolume/data/trackruns.txt; then
    echo "Key demo action database engine not enabled. Prepping database..."
    echo ""

    # Perform some quick house keeping to ensure postgres exists and if it is running
    # If postgres does not exists print error and quit
    # If postgres is not running, start it up

    # Postgres exist?
    command -v psql >/dev/null
    if [ $? == 1 ]; then
        echo "---------------------------ERROR----------------------------"
        echo "--  POSTGRE SQL NOT INSTALLED / PLEASE INSTALL AND RETRY  --"
        echo "------------------------------------------------------------"
        sed -i '' '/^2/d' trackruns.txt
        sleep 2
        exit
    fi

    # Postgres running?
    pgrep postgres >/dev/null
    if [ $? == 1 ]; then
        pg_ctl -D /usr/local/var/postgres start
    fi

    # Check if requested database (in variable $DATABASE) exists. If not create it
    psql -lt | cut -d \| -f 1 | grep -wq $DATABASE
    if [ $? == 1 ]; then
        createdb -O $DBUSER $DATABASE
    fi

    # Set up Vault database engine and required elements required for demo (schema, tables, roles)
    vault secrets enable database >/dev/null
    psql -U $DBUSER -h localhost -d $DATABASE -c "CREATE SCHEMA thelab" >/dev/null
    psql -U $DBUSER -h localhost -d $DATABASE -c "CREATE TABLE thelab.labssn ( id serial PRIMARY KEY, social_security_number VARCHAR UNIQUE NOT NULL)" >/dev/null
    psql -U $DBUSER -h localhost -d $DATABASE -c "ALTER ROLE $DBUSER SET search_path TO $DATABASE,public" >/dev/null
    vault write database/config/$DATABASE plugin_name=postgresql-database-plugin allowed_roles="readonly, write" connection_url=postgresql://{{username}}:{{password}}@host.docker.internal:5432/$DATABASE?sslmode=disable username=$DBUSER password=$DBPASS
    vault write database/roles/readonly db_name=$DATABASE creation_statements=@vdemofiles/readonly.sql default_ttl=5s max_ttl=1h
    osascript -e "tell application \"Terminal\" to activate" -e "tell application \"Terminal\" to do script \"cd $(pwd); vscripts/psqlwatch.sh\""
    sleep 2

    # DB account needs to be set in vdemofiles/revoke.sql for transit-db to work. This checks
    # the $DBUSER variable and sets appropriately in the referenced file.
    grep $DBUSER vdemofiles/revoke.sql >/dev/null
    if [ $? == 1 ]; then
        echo "Updating revoke.sql for demo requirements..."
        sed -i '' "s/dbuser/$DBUSER/g" vdemofiles/revoke.sql
    fi
    clear
fi

# Showcase end to end use of Vault
echo "END TO END OPERATIONS DEMO - TRANSIT WITH DATABASE"
echo "--------------------------------------------------------------"
echo ""
echo "Across this demo we've shown static, dynamic secrets, encryption as a service and methods of authentication."
echo "This sections walks you through a scenario use case of using Vault in an operational aspect."
echo ""
echo "In this scenario our lab manager Bunsen Honeydew needs to update his \"$DATABASE\" database with social security numbers."
echo "However he has a confidentiality concerns and wants to encrypt them at rest."
echo ""
echo "The following workflow will be followed:"
echo ""
echo "1) User \"$USER1\" will authenticate to Vault and generate a token for use"
echo "2) A new database user for the database \"$DATABASE\" will be generated dynamically with a TTL of 30 seconds"
echo "3) Bunsen will send his social security numbers to Vault using the transit engine to encrypt the data"
echo "4) The newly created database user will insert the encrypted data into the table"
echo ""
echo ""
echo "The list of SSN that will be encrypted is:"
echo ""
cat vdemofiles/labssn.txt
echo ""
echo ""
echo "We'll also open up another terminal where we can watch real time the database being used."

osascript -e "tell application \"Terminal\" to activate" -e "tell application \"Terminal\" to do script \"cd $(pwd); vscripts/psqltransit.sh\""

read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "EXECUTE WORKFLOW"
echo "--------------------------------------------------------------"

echo "The first thing that needs to happen is the database engine needs to be updated to allow writes on the $DATABASE database."
echo "The role will be created on the database, and then the policy for writing will be set with a TTL of 20s for the created user."
echo ""
echo ""
echo "The role's SQL statement that Vault will use is:"
echo ""
cat vdemofiles/write3.sql
echo ""
echo ""
echo "$TCOLOR vault write database/roles/write db_name=$DATABASE creation_statements=@vdemofiles/write3.sql revocation_statements=@vdemofiles/revoke.sql default_ttl=20s max_ttl=1h" | $TYPE
tput sgr0

if ! grep -Fxq "3" vvolume/data/trackruns.txt; then
    echo "Key demo action transit EaaS engine not enabled. Prepping transit engine..."
    vault secrets enable transit
    vault write -f transit/keys/ssn
fi

if ! grep -Fxq "5" vvolume/data/trackruns.txt; then
    echo "Key demo action userpass method not enabled. Prepping authentication engine..."
    vault policy write base vdemofiles/base.hcl
    vault auth enable userpass
    vault write auth/userpass/users/$USER1 password=$USER1PASSWD policies="base"
fi

vault write database/roles/write db_name=$DATABASE creation_statements=@vdemofiles/write3.sql revocation_statements=@vdemofiles/revoke.sql default_ttl=20s max_ttl=1h

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "STEP 1 - GET BUNSEN A TOKEN TO INTERACT WITH VAULT"
echo "--------------------------------------------------------------"
echo "Now that the database engine and role has been configured, Bunsen will login to obtain his token..."
echo "Note, Bunsen decided to login via the API."
echo ""
echo ""
echo "$TCOLOR curl --request POST --data '{\"password\": \"honeydew\"}' \$VAULT_ADDR/v1/auth/userpass/login/$USER1 | jq" | $TYPE
tput sgr0
echo ""
echo ""
#vault login -method=userpass username=$USER1 password=$USER1PASSWD

curl --request POST --data '{"password": "'$USER1PASSWD'"}' $VAULT_ADDR/v1/auth/userpass/login/$USER1 | jq | tee vvolume/data/"$USER1"token.txt
#Export the token for further automated use
export BUNSEN=$(jq -r .auth.client_token vvolume/data/"$USER1"token.txt)

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "STEP 2 - BUNSEN CREATES A NEW DB USER DYNAMICALLY"
echo "--------------------------------------------------------------"
echo "With that token received, Bunsen will execute a request to create a new DB user dynamically that can write the encrypted data into the database."
echo ""
echo ""
echo "$TCOLOR curl --header \"X-Vault-Token: $BUNSEN\" \$VAULT_ADDR/v1/database/creds/write | jq" | $TYPE
#echo "$TCOLOR curl --header \"X-Vault-Token: \$BUNSEN\" \$VAULT_ADDR/v1/database/creds/write | jq" | $TYPE
tput sgr0

curl --header "X-Vault-Token: $BUNSEN" $VAULT_ADDR/v1/database/creds/write | jq | tee vvolume/data/dbwritecreds.txt

#Export the DB WRITE username/password credentials for further automated use
export DBWRITE=$(jq -r .data.username vvolume/data/dbwritecreds.txt)
export DBWRITEPASS=$(jq -r .data.password vvolume/data/dbwritecreds.txt)

echo ""
echo ""
read -rsn1 -p "Demo paused - Press any key to continue..."
clear

echo "STEPS 3 & 4 - BUNSEN OBTAINS CIPHERTEXT AND INSERTS INTO DB"
echo "--------------------------------------------------------------"
echo "Bunsen then loops through his SSN file to convert the plaintext values into base64 encoded values"
echo "and sends the data first to be encrypted by Vault's transit engine."
echo ""
echo "Once the data is encrypted, he receives the ciphertext back from Vault and uses the dynamically"
echo "generated DB user with write permissions to insert it into the database \"$DATABASE\" table \"labssn\" ."
echo ""
echo ""
echo "Dynamically generated database credentials with TTL=20s"
echo "-------------------------------------------------------"
echo "Username: $(jq -r .data.username vvolume/data/dbwritecreds.txt)"
echo "Password: $(jq -r .data.password vvolume/data/dbwritecreds.txt)"
echo ""
echo ""
#echo "$TCOLOR while read LINE; do vault write transit/encrypt/ssn plaintext=\$(base64 <<< \"\$LINE\") | awk 'NR==3{print \$2}'" | $TYPE
echo "$TCOLOR curl --header \"X-Vault-Token: $(jq -r .auth.client_token vvolume/data/"$USER1"token.txt)\" --request POST --data '{\"plaintext\": \"'\"$(base64 <<<$LINE)\"'\"}' $VAULT_ADDR/v1/transit/encrypt/ssn | jq -r .data.ciphertext" | $TYPE
tput sgr0
echo ""
echo ""
#echo "$TCOLOR psql -U $DBWRITE -d $DATABASE -c \"INSERT INTO thelab.labssn (social_security_number) VALUES (ciphertext)\" done <vdemofiles/labssn.txt" | $TYPE
#echo "$TCOLOR psql \"postgresql://\$DBWRITE:\$DBWRITEPASS@localhost/$DATABASE\" -c \"INSERT INTO thelab.labssn (social_security_number) VALUES (ciphertext)\" done <vdemofiles/labssn.txt" | $TYPE
echo "$TCOLOR psql \"postgresql://$(jq -r .data.username vvolume/data/dbwritecreds.txt):$(jq -r .data.password vvolume/data/dbwritecreds.txt)@localhost/$DATABASE\" -c \"INSERT INTO thelab.labssn (social_security_number) VALUES (ciphertext)\" done <vdemofiles/labssn.txt" | $TYPE
tput sgr0
echo ""
echo ""
while read LINE; do
    #export SSN=$(echo $LINE)
    echo ""
    echo "SSN: $LINE - encrypting via Vault transit..."
    #export ENC=$(vault write transit/encrypt/ssn plaintext=$(base64 <<<"$SSN") | awk 'NR==3{print $2}')
    export ENC=$(curl --header "X-Vault-Token: $BUNSEN" --request POST --data '{"plaintext": "'"$(base64 <<<$LINE)"'"}' $VAULT_ADDR/v1/transit/encrypt/ssn | jq -r .data.ciphertext)
    echo "Ciphertext received: $ENC - loading into database..."
    #psql -U $DBWRITE -d $DATABASE -c "INSERT INTO thelab.labssn (social_security_number) VALUES ('$ENC')"
    psql "postgresql://$DBWRITE:$DBWRITEPASS@localhost/$DATABASE" -c "INSERT INTO thelab.labssn (social_security_number) VALUES ('$ENC')"
done <vdemofiles/labssn.txt
echo ""
echo ""
read -rsn1 -p "Demo paused - Check Postgres database \"$DATABASE\" for \"labssn\" table data. Press any key to continue..."
clear

echo "DECRYPT - BUNSEN OBTAINS CIPHERTEXT AND DECRYPTS WITH TRANSIT"
echo "-------------------------------------------------------------"
echo "Decrypting ciphertext uses the same Vault transit engine."
echo "The reverse process is followed by selecting the ciphertext data from the DB and forwarding to Vault to decrypt."
echo ""
echo "The user created earlier to write to the database has been revoked. Using his token Bunsen has Vault dynamically"
echo "generate a read-only user to pull the encrypted data."
echo ""
echo ""
echo "$TCOLOR curl --header \"X-Vault-Token: $(jq -r .auth.client_token vvolume/data/"$USER1"token.txt)\" \$VAULT_ADDR/v1/database/creds/readonly | jq" | $TYPE
tput sgr0
# TESTING REMOVE THIS CODE AFTER RESOLUTION
#curl --request POST --data '{"password": $USER1PASSWD}' $VAULT_ADDR/v1/auth/userpass/login/$USER1 | jq | tee vvolume/data/$USER1token1.txt
#Export the token for further use automated
#export BUNSEN1=$(awk 'NR==10{print $2}' vvolume/data/$USER1token1.txt | tr -d '",')
# END TESTING

curl --header "X-Vault-Token: $BUNSEN" --request GET $VAULT_ADDR/v1/database/creds/readonly | jq -r .data | tee vvolume/data/dbreadcreds.txt

#Export the DB READ username/password credentials for further automated use
export DBREAD=$(jq -r .username vvolume/data/dbreadcreds.txt)
export DBREADPASS=$(jq -r .password vvolume/data/dbreadcreds.txt)

#echo "$TCOLOR psql -U \$DBWRITE -d $DATABASE -c \"SELECT * FROM thelab.labssn\"" | $TYPE
echo "$TCOLOR psql \"postgresql://$(jq -r .username vvolume/data/dbreadcreds.txt):$(jq -r .password vvolume/data/dbreadcreds.txt)@localhost/$DATABASE\" -c \"SELECT * FROM thelab.labssn\"" | $TYPE
tput sgr0
echo ""

#psql -U $DBWRITE -d $DATABASE -c "SELECT * FROM thelab.labssn" | tee vvolume/data/ciphertext.txt
psql "postgresql://$DBREAD:$DBREADPASS@localhost/"$DATABASE"" -c "SELECT * FROM thelab.labssn" | tee vvolume/data/ciphertext.txt

echo ""
echo "The original SSN values we submitted to the database were:"
echo ""

cat vdemofiles/labssn.txt

# echo ""
# echo ""
# echo "The ciphertext we retreived from the database was:"
# echo ""
# echo "$TCOLOR cat vvolume/data/ciphertext.txt" | $TYPE
# tput sgr0
# echo ""
# echo ""
# cat vvolume/data/ciphertext.txt
echo ""
echo ""
echo "In this example we will look to find the plaintext of id 3."
echo "Feeding the ciphertext back to the Vault transit key and decrypting yields a base64 output:"
echo ""
echo ""
#echo "$TCOLOR vault write transit/decrypt/ssn ciphertext=$(awk 'NR==5{print $3}' vvolume/data/ciphertext.txt)" | $TYPE
echo "$TCOLOR curl --header \"X-Vault-Token: $(jq -r .auth.client_token vvolume/data/"$USER1"token.txt)\" --request POST --data '{\"ciphertext\": \"$(awk 'NR==5{print $3}' vvolume/data/ciphertext.txt)\"}' $VAULT_ADDR/v1/transit/decrypt/ssn | jq -r .data.plaintext" | $TYPE
tput sgr0
echo ""

curl --header "X-Vault-Token: $BUNSEN" --request POST --data '{"ciphertext": "'"$(awk 'NR==5{print $3}' vvolume/data/ciphertext.txt)"'"}' $VAULT_ADDR/v1/transit/decrypt/ssn | jq -r .data.plaintext | tee vvolume/data/base64.txt
#export DENC=$(cat vvolume/data/base64.txt)

echo ""
echo "Finally to obtain the SSN value use base64 to decode:"
#echo $DENC
echo ""
echo "$TCOLOR base64 --decode <<< $(cat vvolume/data/base64.txt)"
tput sgr0
base64 --decode <<<$(cat vvolume/data/base64.txt)
#unset DENC

echo ""
echo ""
read -rsn1 -p "This concludes the end-to-end operational use demo. Press any key to return to menu..."
