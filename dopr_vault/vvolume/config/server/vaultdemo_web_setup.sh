#!/bin/bash

# This script runs webserver initial configuration demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

cp /opt/shared/config/server/nginx_supervisord.conf /etc/supervisord.conf
sed -i -e "s/hostname/$(hostname)/g" /etc/supervisord.conf
sed -i -e "s/worker_processes auto;/worker_processes 1;/g" /etc/nginx/nginx.conf
sed -i -e "/user nginx;/a daemon off;" /etc/nginx/nginx.conf
supervisord -c /etc/supervisord.conf
# Have to make /run directory as Alpine Linux doesn't include it in base docker image
mkdir -p /run/nginx
mkdir -p /var/www/catapp
mkdir /etc/nginx/cert
rm /etc/nginx/conf.d/default.conf
cat <<EOF >/var/www/catapp/index.html
<html>
  <head><title>Meow!</title></head>
  <body>
  <div style="width:800px;margin: 0 auto">

  <!-- BEGIN -->
  <center><img src="https://placekitten.com/450/450"></img></center>
  <center><h2>Meow World!</h2></center>
  <center>Welcome to Beau's cat app.</center>
  <center>__________________________</center>
  <center><br>Meow meow do what I want because I'm a cat...</br></center>
  <!-- END -->

  </div>
  </body>
</html>
EOF

#Configure basic webserver listening to INSECURE port first

cat <<EOF >/etc/nginx/conf.d/catapp.conf
server {
	listen $(hostname -i):80;
	root /var/www/catapp;
	index index.html;
}
EOF

# Tell supervisor that Nginx needs to be reloaded to pick up new configuration
supervisorctl reload >/dev/null
