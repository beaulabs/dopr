#!/bin/bash

# This script runs webserver secondary configuration demo as part of the Dopr Vault demo environment
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs

# Ready Nginx to serve up the website securely

cat <<EOM >/etc/nginx/conf.d/catapp.conf
server {
	listen $(hostname -i):80;
	root /var/www/catapp;
	index index.html;
}
server {
    listen $(hostname -i):443;
	ssl on;
	ssl_certificate /etc/nginx/cert/ssl_cert;
	ssl_certificate_key /etc/nginx/cert/key;
        root /var/www/catapp;
        index index.html;
}
EOM

mv /opt/shared/data/ssl_cert /etc/nginx/cert
mv /opt/shared/data/key /etc/nginx/cert
nginx -s reload
