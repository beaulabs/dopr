#!/bin/bash

# This script will run Consul in server mode pulling config file from a shared location
#
# Change the configuration file location to match directory structure required.
# Configuration file location: /Users/beau/labs/beaulabs/dopr/dopr_vaultdemo_content/containerbuild

# Configure CA certificates for using https
cp /opt/shared/certs/_wildcard.vault.test+3.crt /usr/local/share/ca-certificates/
update-ca-certificates

# Start Consul server
cp /opt/shared/config/server/consul_supervisord.conf /etc/supervisord.conf
sed -i -e "s/hostname/$(hostname)/g" /etc/supervisord.conf
supervisord -c /etc/supervisord.conf
