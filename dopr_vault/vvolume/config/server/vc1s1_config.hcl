# Use consul storage
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vaultcluster1/"
}

# Use file based storage
# storage "file" {
#   path = "/opt/shared/data"
# }


# Set the listener address, cluster address and tls certs
listener "tcp" {
  #address       = "127.0.0.1:8200"
  address         = "0.0.0.0:8200"
  cluster_address = "0.0.0.0:8201"
  tls_disable     = false
  tls_cert_file   = "/opt/shared/certs/_wildcard.beaulabs.test+15.pem"
  tls_key_file    = "/opt/shared/certs/_wildcard.beaulabs.test+15-key.pem"
}

# Enable Vault UI
ui = true

# Disable mlock - NOTE only have this setting for dev. Do not run in production
disable_mlock = true

# Advertise non loopback address
api_addr = "https://10.0.1.101:8200"

cluster_addr = "https://10.0.1.101:8201"

# Set plugin directory location for addtl plugins needed
plugin_directory = "/opt/shared/plugins"
