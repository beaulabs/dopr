#!/bin/bash

# This script will run Vault in server mode pulling config file from a shared location
#
# Change the configuration file location to match directory structure required.
# Configuration file location: dopr/dopr_vaultdemo_content/containerbuild

# Configure CA certificates for using https
cp /opt/shared/certs/_wildcard.beaulabs.test+15.crt /usr/local/share/ca-certificates/
update-ca-certificates

# Start Vault server and Consul agent
cp /opt/shared/config/server/vault_supervisord.conf /etc/supervisord.conf
sed -i -e "s/hostname/$(hostname)/g" /etc/supervisord.conf
supervisord -c /etc/supervisord.conf
