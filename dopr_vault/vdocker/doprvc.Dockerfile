# This Dockerfile is for building an Ubuntu based image
# that will run HashiCorp software: Terraform / Vault / Consul / Nomad
FROM alpine:latest

# Update Ubuntu software repository for use in installs and then install 
# required additional utilities and configurations
RUN apk --no-cache add ca-certificates bash supervisor zip curl lsof nginx redis postgresql && \
    wget https://releases.hashicorp.com/vault/1.4.2+ent/vault_1.4.2+ent_linux_amd64.zip && \
    wget https://releases.hashicorp.com/consul/1.7.4+ent/consul_1.7.4+ent_linux_amd64.zip && \
    unzip '*.zip' && \
    mv vault consul /usr/local/bin && \
    rm *.zip 

EXPOSE 8200 8500 80 443 5432 6739
