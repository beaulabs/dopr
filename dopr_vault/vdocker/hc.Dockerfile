# This Dockerfile is for building an Ubuntu based image
# that will run HashiCorp software: Terraform / Vault / Consul / Nomad
FROM ubuntu:latest

# Update Ubuntu software repository for use in installs and then install 
# required additional utilities and configurations
RUN apt-get update && \
    apt-get install -y iproute2 net-tools iputils-ping zip vim jq cron lsof \
    cron ca-certificates openssl wget nginx systemd && \
    wget https://releases.hashicorp.com/vault/1.3.1+ent/vault_1.3.1+ent_linux_amd64.zip && \
    wget https://releases.hashicorp.com/consul/1.6.2+ent/consul_1.6.2+ent_linux_amd64.zip && \
    wget https://releases.hashicorp.com/nomad/0.10.2/nomad_0.10.2_linux_amd64.zip && \
    wget https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip && \
    unzip '*.zip' && \
    mv consul vault nomad terraform /usr/local/bin && \
    rm *.zip && \
    apt-get clean && \
    apt-get autoremove

EXPOSE 80 443 8200 8500

