path "team2apps/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "team2salary/" {
  capabilities = ["list"]
}
