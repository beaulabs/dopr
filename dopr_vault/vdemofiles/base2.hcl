path "labsecrets/lab*" {
  capabilities = ["create", "read"]
}

path "labsecrets/" {
  capabilities = ["list"]
}

