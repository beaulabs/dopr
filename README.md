# Demo Operation Plan Response

"Shall we play a demo?"

In honor of War Games (a fantastic 80's flicker show) this introduces The Demo Operation Plan Response, or DOPR (/'dah-per/). As I run a lot of demos, I needed something that would allow an automated demo of key functionality of my HashiCorp software.

## Latest Version

Version: 1.1

DOPR is a work in progress. It currently supports a Vault demo.

## Purpose

At issue when demoing the HashiCorp software stack: **Vault** / **Consul** / **Terraform** / **Nomad** : is being able to show key components and functionality of it without having to stop and cut/paste CLI or API commands, and open terminals or browser based elements to show realtime updates. Sometimes customers stop and talk through an element at length and it can be difficult to remember where you last were in the demo flow. This helps by injecting key pauses.

Another concern is often when on-site with a customer there is insignificant cell signal, or lack of access to guest wi-fi to run demos from a pre-configured environment running in the cloud. Although some demos will and do require access to the internet, some demos can be run locally from a laptop. Dopr's intent is to cover circumstances where you do not have good cell-coverage, lack of wi-fi access or just need to spin something up quickly to show functionality.

This repo contains scripted Bash code that will allow an automated step by step process of features listed below in **Details**.

**NOTE:** This demo is configured to run locally on an **Apple OSX based system**. Be aware that changes to terminal calls, as well as browser calls, will be required should you have a non-OSX system. Those changes are outside the scope of these instructions.

**NOTE TO THE NOTE:** Also, be aware I am not a software developer, so I used what I knew to hack DOPR together. I'm sure there are more efficient ways to write this and some day I'll get to it :)

## Pre-Requisites

In order for the automated demo to work you must have:

* **Docker Desktop for Mac**
  
  Some elements of DOPR rely on containers to run. You can download it free from: [**Docker Desktop for Mac**](https://hub.docker.com/editions/community/docker-ce-desktop-mac/)

* **Certificates** to use for Vault HTTPS. Use of [**mkcert**](https://github.com/FiloSottile/mkcert) is a great tool to do this.

    To create a certificate for Dopr use if you don't already have one:

        mkcert --install
        mkcert '*.vault.test' localhost 127.0.0.1 ::1

    This will create ***_wildcard.vault.test+3-key.pem*** and ***_wildcard.vault.test+3.pem***. Once
    you have the certs created move them into the **dopr_vault/vvolume/certs/** certs directory:

        mv _wildcard.vault.test+3-key.pem _wildcard.vault.test+3.pem dopr_vault/vvolume/certs/

* **License for Vault and Consul**

  The docker image uses the publically available Vault Enterprise and Consul Enterprise binaries. You will need a license for both. Once you have created the license you create the following files and set them in the license directory:

  * Vault: **dopr_vault/vvolume/license/vault_license.json**
  * Consul: **dopr_vault/vvolume/license/consul.license**

  Please see the ***vault_license_example.json*** and ***consul.license.example*** example files if you need to see the proper format for the license.

  * Also, probably a pretty good idea to put your license files in your .gitignore file to ensure you don't inadvertently post them in your VCS.

* **PostgreSQL database** installed **AND**
  * username/password created with full admin rights to the database. I'm not a DBA, so for simplicity of the demo admin rights are expected and I allow for the superuser to not have to authenticate with password for administrative items (eg. psql -U \<usrename\> -h localhost -d \<database\>)
  
  Although DOPR will create the PostgreSQL database if it doesn't exist, if you already have one created ensure the username created 
  above has ownership of the database.

* **Okta developer account, organization and token**. You can sign up for a free developer account [**here**](https://developer.okta.com/).

  Specifically for Vault to communicate with your development Okta account you'll need to set up a:

  * Development domain (this will be created when you sign up)
  * API token
  * User with a valid email address
  * Group created for Vault
  * User assigned to the group for Vault
  * Push authentication active

* **DNSMasq**

  If you plan to utilize the PKI CA / Website end to end demo you will want to set up DNSMasq for your local domain to have
  your browser trust the certificate and not get a untrusted error when hitting <https://><domain
  
* **Sudo passwordless privileges (OPTIONAL)**

  As part of the PKI-Webserver operational end to end demo element the PKI certificate needs to be installed on localhost.

  For DOPR to inject the PKI certificate in MacOS KeyChain automatically under the covers, the user executing DOPR requires
  it to have passwordless sudo privileges. This can be edited in /etc/sudoers.

  This is **OPTIONAL**. If you chose to not have sudo privileges you will have to manually add the certificate and change it to
  be trusted before hitting the secure site.

## Details

To start the demo open a terminal (iTerm or Terminal), switch into the directory you have cloned the repo into and run:

    ./dopr.sh

Select from the menu which software stack you wish to demo. Key functionality and details of the demos can be found listed below.

### VAULT

Before starting the Vault demo ensure that you have filled out the *GLOBAL VARIALBES* in **dopr_vault/vaultdemo_main.sh**:

* OKTAORG="INSERT OKTA ORG"
* OKTATOKEN="INSERT OKTA TOKEN"
* DATABASE="INSERT DATABASE TO USE"
* DBUSER="INSERT DBUSER"
* DBPASS="INSERT DB USER PASSWORD"

(friendly reminder to blank these values out...well really your Okta info...if you make changes to the code in your own repo.)

If you do not plan on running a particular element of the Vault demo (eg. Okta authentication) it is ok to leave the variable blank.

### Vault Demo Key Functionality

* Static secrets
* Dynamic secrets
* Transit (Encryption as a Service)
* Username/password authentication method
* Okta authentication method with push MFA
* PKI Certificate authority
* End to end operational use with dynamic secrets and transit
* End to end operational use with PKI certificate authority and website
* Namespaces
* Sentinel (Policy as Code)
* Disaster Recovery
