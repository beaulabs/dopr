#!/bin/bash

# Dopr main script
#
# Version: 0.1
#
# Date: 04 Jan 2020
#
# Author: Beau Labs
#
# Description: Please see README.md at https://https://gitlab.com/beaulabs/dopr for full context.
# Dopr provides a menu based method to perform demos of key functionality of the HashiCorp software stack
# Terraform / Vault / Consul / Nomad

#######################
#      FUNCTIONS      #
#######################

terraformdemo() {
    # Placeholder for Terraform demo script call
    clear
    echo "Terraform demo is not yet completed."
    read -rsn1 -p "Press any key to return to main menu..."
}

vaultdemo() {
    cd dopr_vault/
    ./vaultdemo_main.sh
}

consuldemo() {
    # Placeholder for Consul demo script call
    clear
    echo "Consul demo is not yet completed."
    read -rsn1 -p "Press any key to return to main menu..."
}

nomaddemo() {
    # Placeholder for Nomad demo script call
    clear
    echo "Nomad demo is not yet completed."
    read -rsn1 -p "Press any key to return to main menu..."
}

quit() {
    clear
    echo "How about a nice game of chess?"
    echo "No?"
    echo "Have a nice day..."
    sleep 1
    exit
}

#######################
#        MAIN         #
#######################

# Clear the screen to start fresh
clear

# Print menu and case selections, put in while loop until quit is chosen
while [[ $INPUT != [Qq] ]]; do
    clear
    echo "# DOPR #"
    echo "--------"
    echo ""
    echo "Welcome to Demo Operations Plan Response"
    echo "Select Demo Environment:"
    echo ""
    echo "1) Vault"
    echo "2) Consul"
    # echo "3) Terraform"
    # echo "4) Nomad"
    echo "Q) Quit DOPR"
    echo ""
    echo "Enter selection and hit return..."
    read INPUT

    case $INPUT in
    1)
        vaultdemo
        ;;
    2)
        consuldemo
        ;;
    3)
        terraformdemo
        ;;
    4)
        nomaddemo
        ;;
    Q | q)
        quit
        ;;
    *)
        echo "Invalid selection. Please choose from the available options."
        sleep 1
        ;;
    esac

done
